﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDArmur : MonoBehaviour {
    public int ap;
    public int getAP()
    {
        return ap;
    }
    public void setAP(int value)
    {
        ap = value;
        if (ap < 1)
        {
            ap = 0;
        }
    }
}
