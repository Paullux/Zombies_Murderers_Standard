﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMinimap : MonoBehaviour {

    [SerializeField] Transform target;
    void Start()
    {
    }


    void LateUpdate ()
    {
        transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
        transform.rotation = Quaternion.Euler(target.transform.eulerAngles.x, target.transform.eulerAngles.y, target.transform.eulerAngles.z);
    }
}
