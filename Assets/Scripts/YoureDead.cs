﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YoureDead : MonoBehaviour {
    public int currentLanguage;
    public AudioSource source;
    public AudioClip youreDeadFr, youreDeadEn, youreDeadEs;
    // Use this for initialization
    void Start()
    {
        currentLanguage = PlayerPrefs.GetInt("currentLanguage");
        source = GetComponent<AudioSource>();
        switch (currentLanguage)
        {
            case 0:
                source.PlayOneShot(youreDeadFr, 1.0f);
                break;
            case 1:
                source.PlayOneShot(youreDeadEn, 1.0f);
                break;
            case 2:
                source.PlayOneShot(youreDeadEs, 1.0f);
                break;
        }
    }
}
