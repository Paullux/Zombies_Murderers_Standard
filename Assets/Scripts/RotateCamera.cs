﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.EventSystems;

public class RotateCamera : MonoBehaviour {

    Vector3 FirstPoint;
    Vector3 SecondPoint;
    float xAngle;
    float yAngle;
    float xAngleTemp;
    float yAngleTemp;
    private float sensitivity;

    void Start()
    {
        Input.gyro.enabled = true;
        xAngle = 0;
        yAngle = 0;
        transform.rotation = Quaternion.Euler(yAngle, xAngle, 0);
        sensitivity = PlayerPrefs.GetFloat("sensitivity");
    }

    void Update()
    {

        if (!Input.gyro.enabled) Input.gyro.enabled = true;

        float x = CrossPlatformInputManager.GetAxis("Vertical2");
        float y = CrossPlatformInputManager.GetAxis("Horizontal2");

        xAngleTemp = xAngle;
        yAngleTemp = yAngle;
        xAngle = xAngleTemp + sensitivity * Mathf.Pow(x*0.5f, 3) + Input.gyro.rotationRateUnbiased.x;
        yAngle = yAngleTemp + sensitivity * Mathf.Pow(y * Screen.height / Screen.width, 3) - Input.gyro.rotationRateUnbiased.y;
        transform.rotation = Quaternion.Euler(-xAngle, yAngle, 0.0f);

        float z = transform.eulerAngles.z;
        transform.Rotate(0, 0, -z);
    }
}
