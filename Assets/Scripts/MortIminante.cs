﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MortIminante : MonoBehaviour {

    public Image MonImage;
    //public GameObject Joueur;
    private float PTV;
    private float PerCentOfLife;
    private bool Mort;
    Menu Menu_Script;
    void Start()
    {
        PTV = 100;
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
        //MonImage = GetComponent<Image>();
        var tempColor = MonImage.color;
        tempColor.a = 1f;
        MonImage.color = tempColor;

    }
	
	void Update ()
    {
        PTV = (float)Menu_Script.HealthNumberCell[15];
        PerCentOfLife = 1 - PTV / 100;
        if (PTV <= 0)
        {
            PerCentOfLife = 1f;
            gameObject.GetComponent<Image>().enabled = false;
            Menu_Script.HealthNumberCell[15] = 0;
        }
        var tempColor = MonImage.color;
        tempColor.a = PerCentOfLife;
        MonImage.color = tempColor;
    }
}
