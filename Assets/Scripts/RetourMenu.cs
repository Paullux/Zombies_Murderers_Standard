﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class RetourMenu : MonoBehaviour {

    public double AngleDeRotation;
    Menu Menu_Script;
    void Start()
    {
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }
    public void Update () {
        if (CrossPlatformInputManager.GetButtonDown("Quit") || Input.GetKey(KeyCode.Escape))
        {
            PlayerPrefsX.SetIntArray("HealthType", Menu_Script.HealthTypeCell);
            PlayerPrefsX.SetIntArray("HealthNumber", Menu_Script.HealthNumberCell);
            PlayerPrefsX.SetIntArray("ArmorType", Menu_Script.ArmorTypeCell);
            PlayerPrefsX.SetIntArray("ArmorNumber", Menu_Script.ArmorNumberCell);
            PlayerPrefsX.SetIntArray("GunType", Menu_Script.GunTypeCell);
            PlayerPrefsX.SetIntArray("GunNumber", Menu_Script.GunNumberCell);
            SceneManager.LoadScene(0);
        }
        bool reset = CrossPlatformInputManager.GetButtonDown("Reset");
        if (reset)
        {
            int currentScene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentScene);
        }
    }
}
