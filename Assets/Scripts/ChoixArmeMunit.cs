﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixArmeMunit : MonoBehaviour {

    private int CoupFeu;
    public GameObject Armes;
    //liés à la progressbar
    public float progress = 0.0f;
    public Texture2D emptyProgressBar; // Set this in inspector.
    public Texture2D fullProgressBar; // Set this in inspector.
    public Vector2 pos = new Vector2(20, 40);
    public Vector2 size = new Vector2(60, 20);
    public bool StartBar = false;
    public float TempsDepart = 0f;
    public Scrollbar ProgressBarre;
    Menu Menu_Script;

    void start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
        //Menu_Script.UpdateTXTGun(15, Menu_Script.ArmorNumberCell[15].ToString() + "%");
    }

    void update()
    {
        int InChargeur = Menu_Script.GunNumberCell[14];
        int Balles = Menu_Script.GunTypeCell[14];
        int Bullets = Menu_Script.GunTypeCell[15];

        if (Armes.name == "MunitionsPistolet")
        {
            string EnCharge = InChargeur.ToString() + "/" + Balles.ToString();
            Menu_Script.UpdateTXTGun(14, EnCharge);
        }

        if (Armes.name == "MunitionsFusil")
        {
            Menu_Script.UpdateTXTGun(15, Bullets.ToString());
        }
        //Liés à la progressbar
        progress = 0f;
        Debug.Log("DansChargeur : " + InChargeur.ToString());
        Debug.Log("En Réserve : " + Balles.ToString());
        Debug.Log("Munit Fusion : " + Bullets.ToString());

        if (Armes.name == "MunitionsPistolet" && (InChargeur + Balles) <= 1000) progress = (float)(InChargeur + Balles) / 1000;
        if (Armes.name == "MunitionsFusil" && Bullets <= 500) progress = (float)Bullets / 500;
        if (Armes.name == "MunitionsPistolet" && (InChargeur + Balles) > 1000) progress = 1;
        if (Armes.name == "MunitionsFusil" && Bullets > 500) progress = 1;

        ProgressBarre.size = progress;
    }
}
