﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerreArmure : MonoBehaviour {

    public Image MonImage;
    private float pta;
    private float PerCentOfArmore;
    private bool Protected;
    Menu Menu_Script;
    void Start()
    {
        pta = 0;
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
        //MonImage = GetComponent<Image>();
        var tempColor = MonImage.color;
        tempColor.a = 1f;
        MonImage.color = tempColor;

    }

    void Update()
    {
        pta = (float)Menu_Script.ArmorNumberCell[15];
        PerCentOfArmore = pta / 800;
        if (pta <= 0) PerCentOfArmore = 0f;
        var tempColor = MonImage.color;
        tempColor.a = PerCentOfArmore;
        MonImage.color = tempColor;
    }
}
