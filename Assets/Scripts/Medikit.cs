﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medikit : MonoBehaviour {

    private bool HealthPlus = false;
    public GameObject joueur;
    public GameObject medikit;
    public int AddPTV, typeH;
    private int ptv, newPTV;
    Menu Menu_Script;
    // Update is called once per frame

    void Start()
    {
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }
    
    void Update () {
		if (HealthPlus)
        {
            ptv = Menu_Script.HealthNumberCell[15];

            if (ptv < 100)
            {
                newPTV = AddPTV + ptv;
                if (newPTV > 100) newPTV = 100;
                Menu_Script.HealthNumberCell[15] = newPTV;
                HealthPlus = false;
            }
            else
            {
                for (int cell = 0; cell < 15; cell++)
                {
                    if (Menu_Script.HealthTypeCell[cell] == 0 || Menu_Script.HealthTypeCell[cell] == typeH)
                    {
                        Menu_Script.HealthTypeCell[cell] = typeH;
                        if (Menu_Script.HealthNumberCell[cell] < 5)
                        {
                            Menu_Script.HealthNumberCell[cell] += 1;
                            Menu_Script.UpdateTXTHealth(cell, Menu_Script.HealthNumberCell[cell].ToString() + "/5");
                            Menu_Script.UpdateTXTHealth(15, "");
                            Menu_Script.UpdateImageHealth(cell, typeH, true);
                            HealthPlus = false;
                            break;
                        }
                    }
                }
            }
            Destroy(medikit);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun" || other.gameObject.tag == "Player")
        {
            HealthPlus = true;
        }
    }
}
