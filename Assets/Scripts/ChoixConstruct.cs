﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixConstruct : MonoBehaviour {

    public GameObject Blank;
    public GameObject Construc1;
    public GameObject Construc2;
    public GameObject Construc3;
    public Text TauxConstruc;
    public int Cell;
    private int TypeCell, vide, CoupFeu;

    //liés à la progressbar
    public bool StartBar = false;


    void Start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
    }
    void OnEnable()
    {
        TypeCell = PlayerPrefs.GetInt("C" + Cell.ToString() + "Type");
        vide = PlayerPrefs.GetInt("C" + Cell.ToString() + "Number");

        if (vide == 0) PlayerPrefs.SetInt("C" + Cell.ToString() + "Type", 0);
        
        if (TypeCell == 0)
        {
            Blank.SetActive(true);
            Construc1.SetActive(false);
            Construc2.SetActive(false);
            Construc3.SetActive(false);
        }
        if (TypeCell == 1)
        {
            Blank.SetActive(false);
            Construc1.SetActive(true);
            Construc2.SetActive(false);
            Construc3.SetActive(false);
        }
        if (TypeCell == 2)
        {
            Blank.SetActive(false);
            Construc1.SetActive(false);
            Construc2.SetActive(true);
            Construc3.SetActive(false);
        }
        if (TypeCell == 3)
        {
            Blank.SetActive(false);
            Construc1.SetActive(false);
            Construc2.SetActive(false);
            Construc3.SetActive(true);
        }
        TauxConstruc.text = "";

        if (TypeCell != 0) TauxConstruc.text = vide + "/5";
    }

    void Update()
    {
        //Liés à la progressbar
        if (gameObject.GetComponentInParent<Image>().isActiveAndEnabled && StartBar && Input.GetKeyDown((KeyCode)CoupFeu))
        {
            ChangeWeapon();
        }
    }

    public void BarreStart()
    {
        StartBar = true;
    }

    public void BarreStop()
    {
        StartBar = false;
    }

    //Choix de la Langue
    public void ChangeWeapon()
    {
        if (PlayerPrefs.GetInt("C" + Cell.ToString() + "Number") != 0)
        {
            vide = PlayerPrefs.GetInt("C" + Cell.ToString() + "Number") - 1;
            PlayerPrefs.SetInt("C" + Cell.ToString() + "Number", vide);
            TauxConstruc.text = vide + "/5";
        }
    }
}