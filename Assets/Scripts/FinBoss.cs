﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinBoss : MonoBehaviour {
    public GameObject ZOMBIE, Zombie1, Zombie2, Zombie3, Zombie4, Zombie5, Zombie6, Zombie7, Zombie8;
    private int BossZombiePV;
    private bool fin = false, OneTime = true;
    public GameObject LumiereSphere, AvantExploision, Explosion, MortBoss, FeuDArtifice, Youwin;
	// Use this for initialization
	void Start () {
        AvantExploision.SetActive(false);
        Explosion.SetActive(false);
        FeuDArtifice.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
       BossZombiePV = GetComponent<PointDeVie>().getHP();
       if (BossZombiePV <= 0 && OneTime)
        {
            Zombie1.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie1.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie1.transform.GetChild(0).gameObject.SetActive(false);
                Zombie1.transform.GetChild(1).gameObject.SetActive(true);
                Zombie1.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie2.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie2.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie2.transform.GetChild(0).gameObject.SetActive(false);
                Zombie2.transform.GetChild(1).gameObject.SetActive(true);
                Zombie2.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie3.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie3.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie3.transform.GetChild(0).gameObject.SetActive(false);
                Zombie3.transform.GetChild(1).gameObject.SetActive(true);
                Zombie3.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie4.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie4.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie4.transform.GetChild(0).gameObject.SetActive(false);
                Zombie4.transform.GetChild(1).gameObject.SetActive(true);
                Zombie4.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie5.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie5.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie5.transform.GetChild(0).gameObject.SetActive(false);
                Zombie5.transform.GetChild(1).gameObject.SetActive(true);
                Zombie5.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie6.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie6.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie6.transform.GetChild(0).gameObject.SetActive(false);
                Zombie6.transform.GetChild(1).gameObject.SetActive(true);
                Zombie6.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie7.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie7.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie7.transform.GetChild(0).gameObject.SetActive(false);
                Zombie7.transform.GetChild(1).gameObject.SetActive(true);
                Zombie7.transform.GetChild(2).gameObject.SetActive(false);
            }
            Zombie8.GetComponent<PointDeVie>().setHP(-5);
            if (Zombie8.transform.GetChild(2).gameObject.activeSelf)
            {
                Zombie8.transform.GetChild(0).gameObject.SetActive(false);
                Zombie8.transform.GetChild(1).gameObject.SetActive(true);
                Zombie8.transform.GetChild(2).gameObject.SetActive(false);
            }
            OneTime = false;
            if (fin == false) StartCoroutine("FinBossZombie");
        }
	}
    IEnumerator FinBossZombie()
    {
        fin = true;
        LumiereSphere.SetActive(false);
        AvantExploision.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        MortBoss.GetComponent<MeshRenderer>().enabled = false;
        Explosion.SetActive(true);
        yield return new WaitForSeconds(2f);
        AvantExploision.SetActive(false);
        MortBoss.SetActive(false);
        FeuDArtifice.SetActive(true);
        yield return new WaitForSeconds(1f);
        Youwin.SetActive(true);
        yield return new WaitForSeconds(8f);
        Youwin.SetActive(false);
    }
}
