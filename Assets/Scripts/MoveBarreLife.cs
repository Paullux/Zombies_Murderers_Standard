﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBarreLife : MonoBehaviour {
    public GameObject PlayerHead;
    private Vector3 wantedPosition;
    private Vector3 newWantedPosition;
    void FixedUpdate()
    {
        wantedPosition = PlayerHead.transform.position;
        newWantedPosition = new Vector3(wantedPosition.x, 0f, wantedPosition.z);
        transform.LookAt(newWantedPosition);
    }
}
