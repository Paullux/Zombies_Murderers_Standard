﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiesOrHumain : MonoBehaviour {

    public GameObject ZombieIci;
    public GameObject HumainIci;
    public bool humain;

    void Start ()
    {
        humain = false;
    }

    void Update () {
        if (PlayerPrefs.GetInt("NuitNoir") == 1 && !humain)
        {
            ZombieIci.SetActive(true);
            HumainIci.SetActive(false);
        }
        else
        {
            ZombieIci.SetActive(false);
            HumainIci.SetActive(true);
        }
        if (ZombieIci.activeSelf)
        {
            if (GetComponentInChildren<PointDeVie>().getHP() <= 0)
            {
                humain = true;
            }
        }
	}
}
