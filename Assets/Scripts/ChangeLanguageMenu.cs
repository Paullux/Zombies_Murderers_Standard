﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLanguageMenu : MonoBehaviour {
    public Text Inventaire01;
    public Text Inventaire02;
    public Text Inventaire03;
    public Text Inventaire04;
    private string attack;
    private string armor;
    private string health;
    private string build;
    public GameObject Language;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        attack = Language.GetComponent<xmlReader>().attack;
        armor = Language.GetComponent<xmlReader>().armor;
        health = Language.GetComponent<xmlReader>().health;
        build = Language.GetComponent<xmlReader>().build;
        Inventaire01.text = attack;
        Inventaire02.text = armor;
        Inventaire03.text = health;
        Inventaire04.text = build;
    }
}
