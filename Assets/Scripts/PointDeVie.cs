﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDeVie : MonoBehaviour {
    public int hp;
    public GameObject Tutoriel;
    private bool OneTime = true;
    public int getHP()
    {
    return hp;
    }
    public void setHP(int value)
    {
    hp = value;
        if (hp < 1)
        {
            Tutoriel.GetComponent<Tutorial>().SetTuto(4);
            if (OneTime)
            {
                int nbzo = GetComponentInParent<CountOfZombies>().getNZR();
                nbzo += 1;
                GetComponentInParent<CountOfZombies>().setNZR(nbzo);
                OneTime = false;
            }
        hp = 0;
        }
    }
}
