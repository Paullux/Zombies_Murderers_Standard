﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    [SerializeField] Transform target;
    void Start()
    {
    }


    void LateUpdate ()
    {
        transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
        float angle = target.transform.eulerAngles.y;
        transform.rotation = Quaternion.Euler(90, angle, 0);
    }
}
