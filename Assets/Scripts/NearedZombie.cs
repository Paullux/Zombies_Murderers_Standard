﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NearedZombie : MonoBehaviour
{
    private float dist;
    private float mindist;
    private GameObject goodhuman;

    void start()
    {
        
    }

    void Update()
    {

        mindist = Mathf.Infinity;
        GameObject[] humans = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (GameObject human in humans)
        {
            if (human.name == "CaucasianMale")
            {
                dist = Vector3.Distance(human.transform.position, transform.position);
                if (dist < mindist)
                {
                    mindist = dist;
                    goodhuman = human;
                }
            }
        }
        if (Input.GetKey((KeyCode)PlayerPrefs.GetInt("followInt")) && goodhuman.activeSelf) goodhuman.GetComponent<MoveHuman>().setFollowMe();
    }
}