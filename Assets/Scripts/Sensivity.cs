﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sensivity : MonoBehaviour {
    private float sensitivity;
    public Text Sensitive;
    public GameObject param;
	// Use this for initialization
	void Start () {
        sensitivity = PlayerPrefs.GetFloat("sensitivity", 5);
	}
	
	// Update is called once per frame
	void Update () {
        if (param.activeSelf)
        Sensitive.text = sensitivity.ToString();
	}
    public void plus ()
    {
        sensitivity += 1;
        if (sensitivity == 0) sensitivity = 1;
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }
    public void moins()
    {
        sensitivity -= 1;
        if (sensitivity == 0) sensitivity = -1;
        PlayerPrefs.SetFloat("sensitivity", sensitivity);
    }

}
